import { createElement, addClass, removeClass } from './helper.mjs';
import { MAX_PROGRESS_BAR_WIDTH } from './config.mjs';
import { texts } from './data.mjs';

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("http://localhost:3003/game", { query: { username } });

let activeRoomName = null;
let cursor = 0;

const setActiveRoomName = roomName => {
  activeRoomName = roomName;
};

const changePageView = () => {
  roomsPage.classList.toggle('display-none');
  gamePage.classList.toggle('display-none');
}

const body = document.querySelector('body');
const gamePage = document.getElementById('game-page');
const roomsPage = document.getElementById('rooms-page');
const roomCards = document.getElementById('rooms');
const roomTitle = document.querySelector('.room-title');
const usersList = document.getElementById('users-list');
const readyButton = document.querySelector('.ready-button');
const timePanel = document.getElementById('timer-to-start');
const gameTimer = document.getElementById('game-timer');
const gameText = document.getElementById('game-text');
const gameTextHighlighted = document.getElementById('game-text-highlighted');

const onClickJoinButton = (roomName) => {
  changePageView();

  setActiveRoomName(roomName);

  roomTitle.innerText = 'Room ' + roomName;

  socket.emit("join_room", roomName);
};

const onClickBackButton = () => {
  changePageView();
  const roomName = activeRoomName;

  socket.emit("exit_room", roomName);

  activeRoomName = null;
};

const createUserItem = (player) => {
  const { username, isReady } = player;
  const indicatorDiv = createElement({
    tagName: "div",
    className: "ready-indicator",
  });

  const usernameSpan = createElement({
    tagName: "span",
    className: "username",
  });

  usernameSpan.innerText = username;

  if(username === sessionStorage.getItem("username")) {
    usernameSpan.innerText += " (you)";
  }

  const progressDivContainer = createElement({
    tagName: "div",
    className: "progress-bar-container",
  });

  const progressDiv = createElement({
    tagName: "div",
    className: "progress-bar",
    attributes: { style: `width: ${player.progressBarWidth}px` },
  });

  const userListItem = createElement({
    tagName: "li",
    className: isReady ? "user ready" : "user not-ready",
  });

  progressDivContainer.append(progressDiv);
  userListItem.append(indicatorDiv, usernameSpan, progressDivContainer);

  return userListItem;
}

const onClickCreateRoomButton = () => {
  const roomName = prompt("Enter the room name:");
  if (!roomName) {
    return;
  }
  
  socket.emit("create_room", roomName);
};

const addRoom = ({ roomName, howManyPlayers }) => {
  const roomCard = createRoomCard(roomName, howManyPlayers);
  roomCards.append(roomCard);
}

const createRoomCard = (roomName, howManyPlayers) => {
  const roomCard = createElement({
    tagName: "div",
    className: "room-card",
  });

  roomCard.dataset.name = roomName;

  const connectedPlayers = createElement({
    tagName: "span",
    className: "connected-players",
  })

  connectedPlayers.innerText = howManyPlayers + " players connected";

  const roomCardName = createElement({
    tagName: "h3",
    className: "room-name",
  });

  roomCardName.innerText = "Room " + roomName;

  const roomJoinButton = createElement({
    tagName: "button",
    className: "submit-button join-button",
  });

  roomJoinButton.innerText = "Join";

  roomCard.append(connectedPlayers, roomCardName, roomJoinButton);

  return roomCard;
};

body.addEventListener('click', (event) => {
  const target = event.target;

  if(target.classList.contains('create-room-button')) {
    onClickCreateRoomButton();
  }

  if(target.classList.contains('join-button')) {
    const roomName = target.parentNode.dataset.name;
    onClickJoinButton(roomName);
  };

  if(target.classList.contains('back-button')) {
    onClickBackButton();
  }
  
  if(target.classList.contains('ready-button')) {
    onClickReadyButton(target);
  }
});

const onClickReadyButton = (target) => {
  if(target.innerText === "READY") {
    target.innerText = "NOT READY";
  } else {
    target.innerText = "READY";
  }

  socket.emit("get_ready", activeRoomName);
}

const updateRooms = (rooms) => {
  const allRooms = Object.keys(rooms).map(room => createRoomCard(room, rooms[room].length));
  roomCards.innerHTML = "";
  roomCards.append(...allRooms);
}

const updateAllPlayers = (players) => {
  const allPlayersItems = players.map(createUserItem);
  usersList.innerHTML = "";
  usersList.append(...allPlayersItems);
}

const alertRoomNameError = (roomName) => {
  alert(roomName + ' is already taken. Please choose another name.');
  onClickCreateRoomButton();
}

const runTimerBeforeStart = (countdownTime) => {
  addClass(readyButton, "display-none");
  removeClass(timePanel, "display-none");
  timePanel.innerText = countdownTime;
};

const startGame = ({ roomName, randomIndex, timeLeft }) => {
  addClass(timePanel, "display-none");
  gameTimer.innerText = timeLeft + ' seconds left';

  const text = texts[randomIndex];
  gameText.innerText = text;
  gameTextHighlighted.innerText = "";

  let cursor = 0;
  document.addEventListener("keypress", pressKey.bind(event, text, roomName)); 
};

const runGameTimer = ({ roomName, timeLeft}) => {
  gameTimer.innerText = timeLeft + ' seconds left';
};

const finishGame = ({ rooms, players, winner}) => {
  alert(winner.username + ' WINS!');
  document.removeEventListener("keypress", pressKey);

  gameTimer.style.display = "none";
  gameText.innerText = "";
  gameTextHighlighted.innerText = "";
  updateAllPlayers(players.map(player => {
    player.isReady = !player.isReady;
    player.progressBarWidth = 0;
    return player;
  }));
  updateRooms(rooms);
  removeClass(readyButton, "display-none");
};

const pressKey = (text, roomName, event) => {
  if (cursor < text.length && event.keyCode === text[cursor].charCodeAt(0)) {
    gameText.innerText = text.slice(cursor + 1);
    gameTextHighlighted.innerText = text.slice(0, cursor + 1);

    let progressBarWidth = Math.round(
      (gameTextHighlighted.innerText.length / text.length) *
        MAX_PROGRESS_BAR_WIDTH
    );
    socket.emit("correct_keypress", { roomName, progressBarWidth });

    cursor++;
  }
};
socket.on("update_rooms", updateRooms);
socket.on("exit_room_done",  updateAllPlayers);
socket.on("create_room_done", addRoom);
socket.on("join_room_done", updateAllPlayers);
socket.on("room_name_error", alertRoomNameError);

socket.on("get_ready_done", updateAllPlayers);
socket.on("run_timer_before_start", runTimerBeforeStart);
socket.on("start_game", startGame);
socket.on("run_game_timer", runGameTimer);
socket.on("update_progress_bar", updateAllPlayers);
socket.on("display_winner", finishGame);
