import * as config from './config';
import { texts } from '../public/javascript/data.mjs';
import { MAX_PROGRESS_BAR_WIDTH } from '../public/javascript/config.mjs'

let rooms = {};

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    socket.emit("update_rooms", rooms);

    socket.on("create_room", (roomName) => {
      if (rooms[roomName] !== undefined) {
        socket.emit("room_name_error", roomName);
      } else {
        rooms[roomName] = [];
        const players = rooms[roomName];
        const howManyPlayers = players.length;
        

        socket.emit("create_room_done", { roomName, howManyPlayers });
        socket.broadcast.emit("create_room_done", { roomName, howManyPlayers });
      }
    });

    socket.on("join_room", (roomName) => {
        rooms[roomName].push({ 
            username: username, 
            isReady: false, 
            progressBarWidth: 0,
            resultTime: 0, 
        });
        const players = rooms[roomName];
        
        socket.join(roomName, () => {
            io.to(roomName).emit("join_room_done", players);
        });
    });

    socket.on("exit_room", (roomName) => {
      
        const players = rooms[roomName];
        const index = players.findIndex(player => player.username === username);
        players.splice(index, 1);
  
        io.to(roomName).emit("exit_room_done", players);
        socket.emit("update_rooms", rooms);
        socket.leave(roomName);
    });

    socket.on("get_ready", (roomName) => {
        const players = rooms[roomName];
        const player = players.find(player => player.username === username);
        player.isReady = !player.isReady;
  
        io.to(roomName).emit("get_ready_done", players);

        if (everyPlayerIsReady(players)) {
          runTimerBeforeStart(config, roomName);
        }
      });

      socket.on("correct_keypress", ({ roomName, progressBarWidth }) => {
        const player = rooms[roomName].find((player) => player.username === username);
        player.progressBarWidth = progressBarWidth;
        player.timeFinished = Date.now();

        const players = rooms[roomName];
        io.to(roomName).emit("update_progress_bar", players);
      });

      const runTimerBeforeStart = (config, roomName) => {
        let timeLeft = config.SECONDS_TIMER_BEFORE_START_GAME;
        
        const countdown = setInterval(() => {
          io.to(roomName).emit("run_timer_before_start", timeLeft);
      
          timeLeft--;
          if (timeLeft < 0) {
            clearInterval(countdown);
            startGame(roomName);
          }
        }, 1000);
      }

      const startGame = (roomName) => {
        let timeLeft = config.SECONDS_FOR_GAME;
        const randomIndex = Math.floor(Math.random() * texts.length);
        const players = rooms[roomName];
      
        io.to(roomName).emit("start_game", { roomName, randomIndex, timeLeft });
        
        const runGameTimer = setInterval(() => {
          io.to(roomName).emit("run_game_timer", { roomName, timeLeft });

          timeLeft--;
          console.log(everyPlayerFinished(players))
          if (everyPlayerFinished(players) || timeLeft < 0) {
            console.log("works")
            clearInterval(runGameTimer);
            const winner = defineWinner(players);

            io.to(roomName).emit("display_winner", { rooms, players, winner });
          }
        }, 1000);
      }
      
      socket.on('disconnect', () => {
        const name = Object.keys(rooms).find((roomName) =>
        rooms[roomName].some((user) => user.username === username));

        if (rooms.hasOwnProperty(name)) {
          rooms[name] = rooms[name].filter(
            (user) => user.username !== username
          );
          if (rooms[name].length === 0) {
            delete rooms[name];
          }
        socket.emit("update_rooms", rooms);
      }});
  });
};

const everyPlayerIsReady = (players) => {
  return players.every((player) => player.isReady === true);
};
const everyPlayerFinished = (players) => {
  return players.every((player) => player.progressBarWidth === MAX_PROGRESS_BAR_WIDTH);
}; 

const defineWinner = (players) => {
  const sortedPlayers = players.slice().sort((a, b) => {
    if(a.progressBarWidth === b.progressBarWidth) {
      return a.resultTime > b.resultTime ? 1 : -1;
    }
    return a.progressBarWidth < b.progressBarWidth ? 1 : -1;
  });

  return sortedPlayers.pop();
}

